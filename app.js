
    var Item = function (label, subItems) {
    var self = this;

    self.label = label;
    self.subItems = ko.observableArray(subItems);
    self.showSubItems = ko.observable(false);
    self.hideSubItems = ko.observable(true);
    self.hideArrowBlue = ko.observable(true)

    self.toggleSubList = function () {
    self.showSubItems(!self.showSubItems());
    self.hideSubItems(!self.hideSubItems());
};
};

    var ViewModel = function () {

        $(function () {
            var $sortable = $(".sortable");
            var $subList = $(".sub_list");

            $subList.sortable({
                connectWith: $subList,
                handle: '.double_arrow',

                start: function (event, ui) {
                    ui.item.addClass("highlight");
                },

                stop: function (event, ui) {
                    ui.item.removeClass("highlight");

                    var startParent = ui.item.data("startParent");
                    var endParent = ui.item.parent();

                    if (startParent[0] !== endParent[0]) {
                        var itemData = ui.item.data("ko_item");
                        var sourceList = startParent.data("ko_container");
                        var targetList = endParent.data("ko_container");
                        sourceList.remove(itemData);
                        targetList.push(itemData);
                    }
                }
            }).disableSelection();

            $sortable.sortable({
                connectWith: $sortable,
                handle: '.double_arrow',

                start: function (event, ui) {
                    ui.item.data("startParent", ui.item.parent());
                    ui.item.addClass("highlight");

                    ui.item.find(".blue_double_arrow").css("visibility", "visible");
                    ui.item.find(".arrow").css("visibility", "hidden");
                },

                stop: function (event, ui) {
                    ui.item.removeClass("highlight");

                    var startParent = ui.item.data("startParent");
                    var endParent = ui.item.parent();

                    if (startParent[0] !== endParent[0]) {
                        var itemData = ui.item.data("ko_item");
                        var sourceList = startParent.data("ko_container");
                        var targetList = endParent.data("ko_container");
                        sourceList.remove(itemData);
                        targetList.push(itemData);
                    }

                    ui.item.find(".blue_double_arrow").css("visibility", "hidden");
                    ui.item.find(".arrow").css("visibility", "visible");
                }
            }).disableSelection();
        });


    var self = this;

    self.labels = ko.observableArray([
    new Item('Обязательные для всех', ['Паспорт', 'ИНН', 'Права']),
    new Item('Обязательные для трудоустройства', ['Трудовая книжка', 'СНИЛС', 'Военный билет']),
    new Item('Специальные', ['Справка об отсутствии судимости', 'Документы об образовании', 'Реквизиты карты'])
    ]);
};

    ko.applyBindings(new ViewModel());
